set -x

gcc -O3 MaxMIN.c -o MaxMIN
gcc -O3 SuffMIN.c -o SuffMIN
gcc -O3 MaxMin_min.c -o MaxMin_min
gcc -std=gnu99 -O3 MaxMin_min_rs.c -o MaxMin_min_rs -lm
gcc -std=gnu99 -O3 SuffMIN_rs.c -o SuffMIN_rs -lm
gcc -std=gnu99 -O3 MaxMIN_rs.c -o MaxMIN_rs -lm
gcc -std=gnu99 -O3 MIN_d.c -o MIN_d -lm
gcc -std=gnu99 -O3 Min_min_d.c -o Min_min_d -lm
