TASKS[0]=1024

MACHINES[0]=8
MACHINES[1]=12
MACHINES[2]=16

ALGS[0]=MaxMIN_rs
ALGS[1]=MaxMin_min_rs
ALGS[2]=SuffMIN_rs

EXTRA[0]=20
EXTRA[1]=100

for (( i=0;i<1;i++ )) {
	for (( j=0;j<3;j++ )) {
		for (( k=0;k<3;k++ )) {
			for (( l=0;l<2;l++ )) {
				rm -rf ${ALGS[k]}_${TASKS[i]}_${MACHINES[j]}_${EXTRA[l]}
				mkdir ${ALGS[k]}_${TASKS[i]}_${MACHINES[j]}_${EXTRA[l]}
				
				qsub qsub_${ALGS[k]}_${TASKS[i]}_${MACHINES[j]}_${EXTRA[l]}.sh
			}
		}
	}
}
