#echo "Cleaning..."
rm MaxMin_min_rs

#echo "Compiling..."
gcc -std=gnu99 -g MaxMin_min_rs.c -o MaxMin_min_rs -lm

#echo "Running..."
#./MaxMin_min_rs workloads/instances.2/workload_1024_l_l_m_0.log scenarios/8/s.h.0 16 1 1

./MaxMin_min_rs workloads/instances/workload_1024_l_l_m_0.log scenarios/8/s.h.0 1024 8 60
./MaxMin_min_rs workloads/instances/workload_1024_n_l_m_0.log scenarios/8/s.h.0 1024 8 60


./MaxMin_min_rs workloads/instances/workload_1024_l_l_m_0.log scenarios/8/s.h.0 1024 8 1
./MaxMin_min_rs workloads/instances/workload_1024_n_l_m_0.log scenarios/8/s.h.0 1024 8 1
