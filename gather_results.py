#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  gather_results.py
#
#  Copyright 2014 santiago <santiago@MARVIN>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import sys
import io
import numpy

MAX_SCENARIOS=5
MAX_WORKLOADS=5

ERROR_LEVEL=("l","m","h","n")
PARALLELISM_LEVEL=("l",) #("l","m","h")
ARRIVAL_LEVEL=("l",) #("l","m","h")

def main():
    if len(sys.argv) != 3:
        print("Error: {0} <num_tasks> <num_machines>".format(sys.argv[0]))
        exit(-1)
    
    num_tasks = int(sys.argv[1])
    num_machines = int(sys.argv[2])
    
    results_path = ("SuffMIN_"+str(num_tasks)+"_"+str(num_machines), "MaxMIN_"+str(num_tasks)+"_"+str(num_machines), "MaxMin_min_"+str(num_tasks)+"_"+str(num_machines))
    algorithm = ("SuffMIN", "MaxMIN", "MaxMin_min")

    error_makespan = {}
    error_energy = {}

    relative_error_makespan = {}
    relative_error_energy = {}

    max_makespan = {}
    max_energy = {}

    for a_index in range(len(algorithm)):
        a = algorithm[a_index]
        r = results_path[a_index]

        error_makespan[a] = {}
        error_energy[a] = {}

        relative_error_makespan[a] = {}
        relative_error_energy[a] = {}

        for e_level in ERROR_LEVEL:
            error_makespan[a][e_level] = {}
            error_energy[a][e_level] = {}

            relative_error_makespan[a][e_level] = []
            relative_error_energy[a][e_level] = []

            for p_level in PARALLELISM_LEVEL:
                for a_level in ARRIVAL_LEVEL:
                    for w in range(MAX_WORKLOADS):
                        for s in range(MAX_SCENARIOS):
                            file_key = str(num_tasks) + "_" + e_level \
                                + "_" + p_level + "_" + a_level + "_" \
                                + str(w) + "__" + str(s)

                            file_name = r + "/sol_" + a + "_" + file_key + ".0"

                            #file_name = r + "/sol_" \
                            #   + a + "_" + dimension + "_" + e_level \
                            #   + "_" + p_level + "_" + a_level + "_" \
                            #   + str(w) + "__" + str(s) + ".0"

                            with open(file_name) as result_file:
                                content_raw = result_file.readline();
                                content = content_raw.split(" ")

                                e_makespan = float(content[0].strip())
                                a_makespan = float(content[1].strip())
                                e_energy = float(content[2].strip())
                                a_energy = float(content[3].strip())

                                #print("{0} {1} {2} {3} {4}".format(file_name, e_makespan,a_makespan,e_energy,a_energy))

                                error_makespan[a][e_level][file_key] = (e_makespan, a_makespan, (e_makespan - a_makespan)/e_makespan)
                                error_energy[a][e_level][file_key] = (e_energy, a_energy, (e_energy - a_energy)/e_energy)

                                relative_error_makespan[a][e_level].append(error_makespan[a][e_level][file_key][2])
                                relative_error_energy[a][e_level].append(error_energy[a][e_level][file_key][2])

                                if not (file_key in max_makespan) or max_makespan[file_key] < e_makespan:
                                    max_makespan[file_key] = e_makespan

                                if not (file_key in max_energy) or max_energy[file_key] < e_energy:
                                    max_energy[file_key] = e_energy


    print("RELATIVE MAKESPAN ERROR")
    print("\t", end="")
    for a in algorithm:
        print("{0}\t\t".format(a), end="")
    print("")

    for e_level in ERROR_LEVEL:
        if e_level != 'n':
            print("{0}\t".format(e_level), end="")

            for a in algorithm:
                #print("{0} {1}: {2} {3}".format( a, e_level, \
                #   numpy.mean(relative_error_energy[a][e_level]), numpy.std(relative_error_energy[a][e_level])))

                print("{2:.1f}+/-{3:.1f}\t".format( a, e_level, \
                    numpy.mean(relative_error_makespan[a][e_level])*100, numpy.std(relative_error_makespan[a][e_level])*100), end="")

            print("")

    print("RELATIVE ENERGY ERROR")
    print("\t", end="")
    for a in algorithm:
        print("{0}\t\t".format(a), end="")
    print("")

    for e_level in ERROR_LEVEL:
        if e_level != 'n':
            print("{0}\t".format(e_level), end="")

            for a in algorithm:
                #print("{0} {1}: {2} {3}".format( a, e_level, \
                #   numpy.mean(relative_error_energy[a][e_level]), numpy.std(relative_error_energy[a][e_level])))

                print("{2:.1f}+/-{3:.1f}\t".format( a, e_level, \
                    numpy.mean(relative_error_energy[a][e_level])*100, numpy.std(relative_error_energy[a][e_level])*100), end="")

            print("")

    avg_e_makespan_improv = {}
    avg_a_makespan_improv = {}

    for a in algorithm:
        avg_e_makespan_improv[a] = {}
        avg_a_makespan_improv[a] = {}

        for e_level in ERROR_LEVEL:
            avg_e_makespan_improv[a][e_level] = 0.0
            avg_a_makespan_improv[a][e_level] = 0.0

            for file_name in error_makespan[a][e_level].keys():
                avg_e_makespan_improv[a][e_level] = avg_e_makespan_improv[a][e_level] + \
                    ((max_makespan[file_name] - error_makespan[a][e_level][file_name][0]) / max_makespan[file_name])
                avg_a_makespan_improv[a][e_level] = avg_a_makespan_improv[a][e_level] + \
                    ((max_makespan[file_name] - error_makespan[a][e_level][file_name][1]) / max_makespan[file_name])

            avg_e_makespan_improv[a][e_level] = avg_e_makespan_improv[a][e_level] / len(error_makespan[a][e_level])
            avg_a_makespan_improv[a][e_level] = avg_a_makespan_improv[a][e_level] / len(error_makespan[a][e_level])

    #print(avg_e_makespan_improv)
    #print(avg_a_makespan_improv)

    print("RELATIVE MAKESPAN GAIN")
    print("\t", end="")
    for a in algorithm:
        print("{0}\t\t".format(a), end="")
    for a in algorithm:
        print("{0}\t\t".format(a), end="")
    print("")

    for e_level in ERROR_LEVEL:
        print("{0}\t".format(e_level), end="")
        for a in algorithm:
            print("{0:.1f}\t\t".format(avg_e_makespan_improv[a][e_level]*100), end="")
            
        for a in algorithm:
            print("{0:.1f}\t\t".format(avg_a_makespan_improv[a][e_level]*100), end="")
        print("")

    avg_e_energy_improv = {}
    avg_a_energy_improv = {}

    for a in algorithm:
        avg_e_energy_improv[a] = {}
        avg_a_energy_improv[a] = {}

        for e_level in ERROR_LEVEL:
            avg_e_energy_improv[a][e_level] = 0.0
            avg_a_energy_improv[a][e_level] = 0.0

            for file_name in error_energy[a][e_level].keys():
                avg_e_energy_improv[a][e_level] = avg_e_energy_improv[a][e_level] + \
                    ((max_energy[file_name] - error_energy[a][e_level][file_name][0]) / max_energy[file_name])
                avg_a_energy_improv[a][e_level] = avg_a_energy_improv[a][e_level] + \
                    ((max_energy[file_name] - error_energy[a][e_level][file_name][1]) / max_energy[file_name])

            avg_e_energy_improv[a][e_level] = avg_e_energy_improv[a][e_level] / len(error_energy[a][e_level])
            avg_a_energy_improv[a][e_level] = avg_a_energy_improv[a][e_level] / len(error_energy[a][e_level])

    #print(avg_e_energy_improv)
    #print(avg_a_energy_improv)

    print("RELATIVE ENERGY GAIN")
    print("\t", end="")
    for a in algorithm:
        print("{0}\t\t".format(a), end="")
    for a in algorithm:
        print("{0}\t\t".format(a), end="")
    print("")

    for e_level in ERROR_LEVEL:
        print("{0}\t".format(e_level), end="")
        for a in algorithm:
            print("{0:.1f}\t\t".format(avg_e_energy_improv[a][e_level]*100), end="")
            
        for a in algorithm:
            print("{0:.1f}\t\t".format(avg_a_energy_improv[a][e_level]*100), end="")
        print("")

    return 0

if __name__ == '__main__':
    main()

