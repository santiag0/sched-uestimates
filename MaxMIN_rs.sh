ALGORITHM=MaxMIN_rs

#echo "Cleaning..."
rm ${ALGORITHM}

#echo "Compiling..."
gcc -std=gnu99 -g ${ALGORITHM}.c -o ${ALGORITHM} -lm

#echo "Running..."
#./${ALGORITHM} workloads/instances.2/workload_1024_l_l_m_0.log scenarios/8/s.h.0 16 1 30

./${ALGORITHM} workloads/instances.2/workload_1024_l_l_m_0.log scenarios/8/s.h.0 1024 8 60
./${ALGORITHM} workloads/instances.2/workload_1024_n_l_m_0.log scenarios/8/s.h.0 1024 8 60


#./${ALGORITHM} workloads/instances.2/workload_1024_l_l_m_0.log scenarios/8/s.h.0 1024 8 1
#./${ALGORITHM} workloads/instances.2/workload_1024_n_l_m_0.log scenarios/8/s.h.0 1024 8 1
