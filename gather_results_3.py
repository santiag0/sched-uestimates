#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  gather_results.py
#
#  Copyright 2014 santiago <santiago@MARVIN>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import sys
import io
import numpy

MAX_SCENARIOS=400
MAX_WORKLOADS=50

ALGORITHMS=("MaxMIN", "MaxMin_min", "SuffMIN")

ERROR_LEVEL=("l","m","h")
PARALLELISM_LEVEL=("l",) #("l","m","h")
ARRIVAL_LEVEL=("m","h")

SCENARIO_HETER = ("l", "h")

#sol_${ALG_NAME}_${TASK_NUM}_${ERROR_LEVEL}_${PARALLELISM_LEVEL}_${ARRIVAL_LEVEL}_${NUM_LOAD}__${HETER_LEVEL}_${NUM_SCENARIO}

def main():
    if len(sys.argv) != 3:
        print("Error: {0} <num_tasks> <num_machines>".format(sys.argv[0]))
        exit(-1)

    num_tasks = int(sys.argv[1])
    num_machines = int(sys.argv[2])

    error_makespan = {}
    error_energy = {}

    relative_error_makespan = {}
    relative_error_energy = {}

    max_makespan = {}
    max_energy = {}

    num_tasks = int(sys.argv[1])
    num_machines = int(sys.argv[2])

    MAKESPAN_E_IMPROV = {}
    ENERGY_E_IMPROV = {}
    
    MAKESPAN_A_IMPROV = {}
    ENERGY_A_IMPROV = {}

    for s_heter in SCENARIO_HETER:
        MAKESPAN_E_IMPROV[s_heter] = {}
        ENERGY_E_IMPROV[s_heter] = {}

        MAKESPAN_A_IMPROV[s_heter] = {}
        ENERGY_A_IMPROV[s_heter] = {}

        s_num = 0

        for w_para in PARALLELISM_LEVEL:
            for w_error in ERROR_LEVEL:
                MAKESPAN_E_IMPROV[s_heter][w_error] = {}
                ENERGY_E_IMPROV[s_heter][w_error] = {}

                MAKESPAN_A_IMPROV[s_heter][w_error] = {}
                ENERGY_A_IMPROV[s_heter][w_error] = {}

                for w_arr in ARRIVAL_LEVEL:
                    MAKESPAN_E_IMPROV[s_heter][w_error][w_arr] = {}
                    ENERGY_E_IMPROV[s_heter][w_error][w_arr] = {}

                    MAKESPAN_A_IMPROV[s_heter][w_error][w_arr] = {}
                    ENERGY_A_IMPROV[s_heter][w_error][w_arr] = {}

                    for alg in ALGORITHMS:
                        MAKESPAN_E_IMPROV[s_heter][w_error][w_arr][alg] = []
                        ENERGY_E_IMPROV[s_heter][w_error][w_arr][alg] = []
                        
                        MAKESPAN_A_IMPROV[s_heter][w_error][w_arr][alg] = []
                        ENERGY_A_IMPROV[s_heter][w_error][w_arr][alg] = []

                    for w_num in range(MAX_WORKLOADS):
                        E_MAKESPAN_MAX = 0.0
                        E_ENERGY_MAX = 0.0

                        A_MAKESPAN_MAX = 0.0
                        A_ENERGY_MAX = 0.0

                        MAKESPAN_VALUES = {}
                        ENERGY_VALUES = {}

                        for alg in ALGORITHMS:
                            BASE_PATH = alg + "_" + str(num_tasks) + "_" + str(num_machines)
                            FILE_PATH = "sol_" + alg + "_" + str(num_tasks) + "_" + w_error + "_" + w_para + "_" + w_arr + "_" + str(w_num) + "__" + s_heter + "_" + str(s_num)

                            with open(BASE_PATH + "/" + FILE_PATH) as f:
                                content_raw = f.readline()
                                values = content_raw.strip().split(" ")

                                e_makespan = float(values[0].strip())
                                a_makespan = float(values[1].strip())
                                e_energy = float(values[2].strip())
                                a_energy = float(values[3].strip())

                                MAKESPAN_VALUES[alg] = (e_makespan, a_makespan)
                                ENERGY_VALUES[alg] = (e_energy, a_energy)

                                if (a_makespan > A_MAKESPAN_MAX): A_MAKESPAN_MAX = a_makespan
                                if (a_energy > A_ENERGY_MAX): A_ENERGY_MAX = a_energy

                                if (e_makespan > E_MAKESPAN_MAX): E_MAKESPAN_MAX = e_makespan
                                if (e_energy > E_ENERGY_MAX): E_ENERGY_MAX = e_energy

                        for alg in ALGORITHMS:
                            if (MAKESPAN_VALUES[alg][0] == E_MAKESPAN_MAX):
                                E_M_IMPROV = 0.0
                            else:
                                E_M_IMPROV = (E_MAKESPAN_MAX - MAKESPAN_VALUES[alg][0]) / E_MAKESPAN_MAX

                            if (MAKESPAN_VALUES[alg][1] == A_MAKESPAN_MAX):
                                A_M_IMPROV = 0.0
                            else:
                                #A_M_IMPROV = (A_MAKESPAN_MAX - MAKESPAN_VALUES[alg][1]) / A_MAKESPAN_MAX
                                A_M_IMPROV = (E_MAKESPAN_MAX - MAKESPAN_VALUES[alg][1]) / E_MAKESPAN_MAX

                            if (ENERGY_VALUES[alg][0] == E_ENERGY_MAX):
                                E_E_IMPROV = 0.0
                            else:
                                E_E_IMPROV = (E_ENERGY_MAX - ENERGY_VALUES[alg][0]) / E_ENERGY_MAX

                            if (ENERGY_VALUES[alg][1] == A_ENERGY_MAX):
                                A_E_IMPROV = 0.0
                            else:
                                #A_E_IMPROV = (A_ENERGY_MAX - ENERGY_VALUES[alg][1]) / A_ENERGY_MAX
                                A_E_IMPROV = (E_ENERGY_MAX - ENERGY_VALUES[alg][1]) / E_ENERGY_MAX

                            MAKESPAN_E_IMPROV[s_heter][w_error][w_arr][alg].append(E_M_IMPROV)
                            ENERGY_E_IMPROV[s_heter][w_error][w_arr][alg].append(E_E_IMPROV)

                            MAKESPAN_A_IMPROV[s_heter][w_error][w_arr][alg].append(A_M_IMPROV)
                            ENERGY_A_IMPROV[s_heter][w_error][w_arr][alg].append(A_E_IMPROV)

                        s_num = s_num + 1

    for w_error in ERROR_LEVEL:
        print("=== ERROR {0} =================================".format(w_error))

        print("heter\terror\tarrival\t", end="")
        for alg in ALGORITHMS:
            print("{0}\t\t\t".format(alg), end="")
        print("")

        for s_heter in SCENARIO_HETER:
            for w_para in PARALLELISM_LEVEL:
                for w_arr in ARRIVAL_LEVEL:
                    print("{0}\t{1}\t{2}\t".format(s_heter, w_error, w_arr), end="")

                    for alg in ALGORITHMS:
                        mean_e_makespan = numpy.mean(MAKESPAN_E_IMPROV[s_heter][w_error][w_arr][alg])
                        mean_a_makespan = numpy.mean(MAKESPAN_A_IMPROV[s_heter][w_error][w_arr][alg])
                        mean_e_energy = numpy.mean(ENERGY_E_IMPROV[s_heter][w_error][w_arr][alg])
                        mean_a_energy = numpy.mean(ENERGY_A_IMPROV[s_heter][w_error][w_arr][alg])

                        print("{0:4.1f} {1:4.1f} {2:4.1f} {3:4.1f}\t".format(mean_e_makespan*100, mean_a_makespan*100, mean_e_energy*100, mean_a_energy*100), end="")

                    print("")

        print("\t\tTOTAL\t", end="")
        for alg in ALGORITHMS:
            sum_e_makespan = 0.0
            sum_e_energy = 0.0
            sum_a_makespan = 0.0
            sum_a_energy = 0.0
            count = 0

            for s_heter in SCENARIO_HETER:
                for w_para in PARALLELISM_LEVEL:
                    for w_arr in ARRIVAL_LEVEL:
                        count = count + len(MAKESPAN_E_IMPROV[s_heter][w_error][w_arr][alg])

                        sum_e_makespan = sum_e_makespan + numpy.sum(MAKESPAN_E_IMPROV[s_heter][w_error][w_arr][alg])
                        sum_a_makespan = sum_a_makespan + numpy.sum(MAKESPAN_A_IMPROV[s_heter][w_error][w_arr][alg])
                        sum_e_energy = sum_e_energy + numpy.sum(ENERGY_E_IMPROV[s_heter][w_error][w_arr][alg])
                        sum_a_energy = sum_a_energy + numpy.sum(ENERGY_A_IMPROV[s_heter][w_error][w_arr][alg])

            print("{0:4.1f} {1:4.1f} {2:4.1f} {3:4.1f}\t".format((sum_e_makespan/count)*100, (sum_a_makespan/count)*100, (sum_e_energy/count)*100, (sum_a_energy/count)*100), end="")
        print("")


    return 0

if __name__ == '__main__':
    main()

