set -x

PATH_ALGORITHM=/home/siturria/bitbucket/sched-uestimates/${ALG_NAME}
PATH_SCENARIOS=/home/siturria/bitbucket/sched-uestimates/scenarios/${MACHINE_NUM}
PATH_WORKLOADS=/home/siturria/bitbucket/sched-uestimates/workloads/instances

MAX_WORKLOADS=50

ERROR_LEVEL[0]="l"
ERROR_LEVEL[1]="m"
ERROR_LEVEL[2]="h"
ERROR_LEVEL[3]="n"

PARALLELISM_LEVEL[0]="l"
PARALLELISM_LEVEL[1]="m"
PARALLELISM_LEVEL[2]="h"

ARRIVAL_LEVEL[0]="l"
ARRIVAL_LEVEL[1]="m"
ARRIVAL_LEVEL[2]="h"

HETER_LEVEL[0]="l"
HETER_LEVEL[1]="h"

CANT=0

for (( j0=0; j0<1; j0++ )) { # DIMENSION
	for (( j6=0; j6<2; j6++ )) { # HETER
		for (( j1=0; j1<4; j1++ )) { # ERROR LEVEL
			for (( j2=0; j2<1; j2++ )) { # PARALLELISM
				for (( j3=1; j3<3; j3++ )) { # ARRIVAL
					for (( j4=0; j4<${MAX_WORKLOADS}; j4++ )) {
						time (${PATH_ALGORITHM} ${PATH_WORKLOADS}/workload_${TASK_NUM[j0]}_${ERROR_LEVEL[j1]}_${PARALLELISM_LEVEL[j2]}_${ARRIVAL_LEVEL[j3]}_${j4}.log ${PATH_SCENARIOS}/s.${HETER_LEVEL[j6]}.${CANT} ${TASK_NUM[j0]} ${MACHINE_NUM} ${EXTRA} \
							1> sol_${ALG_NAME}_${TASK_NUM[j0]}_${ERROR_LEVEL[j1]}_${PARALLELISM_LEVEL[j2]}_${ARRIVAL_LEVEL[j3]}_${j4}__${HETER_LEVEL[j6]}_${CANT} \
							2> sol_${ALG_NAME}_${TASK_NUM[j0]}_${ERROR_LEVEL[j1]}_${PARALLELISM_LEVEL[j2]}_${ARRIVAL_LEVEL[j3]}_${j4}__${HETER_LEVEL[j6]}_${CANT}.sol) \
							  2> sol_${ALG_NAME}_${TASK_NUM[j0]}_${ERROR_LEVEL[j1]}_${PARALLELISM_LEVEL[j2]}_${ARRIVAL_LEVEL[j3]}_${j4}__${HETER_LEVEL[j6]}_${CANT}.time
							  
						CANT=$((CANT+1))
					}
				}
			}
		}
		
		CANT=0
	}
}

