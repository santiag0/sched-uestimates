// Max-Min energy-aware scheduler.
// Phase 1: Pair with minimum  ETC
// Phase 2: Minimum ETC.
// Parameters : <instance_ETC_file> <num_tasks> <num_machines>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <math.h>
#include <time.h>

#define NO_ASIG -1
#define SIZE_NOM_ARCH 180

#define DEBUG 0
#define SHOW_SOLUTION 0
#define DO_VALIDATE 0

void validate_solution(int NT, int NM, float *E_IDLE, float *E_MAX, int *cores, float **ETC, float **ATC,
                       int *arrival_time, float **mach, int **mach_cores, int *tasks_start, int **num_tasks_cores,
                       int **exec_task_cores, int ***assig_tasks_cores) {

    for (int t = 0; t < NT; t++) {
        int count;
        count = 0;

        int first_m, first_c;

        for (int m = 0; m < NM; m++) {
            for (int c = 0; c < cores[m]; c++) {
                for (int p = 0; p < num_tasks_cores[m][c]-1; p++) {
                    int t2;
                    t2 = assig_tasks_cores[m][c][p];

                    if (t2 == t) {
                        if (count == 0) {
                            first_c = c;
                            first_m = m;
                        } else {
                            printf("[ERROR] Task %d is assigned to %d core %d and to %d core %d (count=%d)\n",
                                   t, first_m, first_c, m, c, count);
                        }

                        count++;
                    }
                }
            }
        }
    }

    for (int i = 0; i < NT; i++) {
        if (tasks_start[i] < arrival_time[i]) {
            printf("[ERROR] Task %d starts on %d but arrives on %d\n",
                   i, tasks_start[i], arrival_time[i]);
        }
    }

    for (int m = 0; m < NM; m++) {
        for (int c = 0; c < cores[m]; c++) {
            for (int p = 0; p < num_tasks_cores[m][c]-1; p++) {
                int t;
                t = assig_tasks_cores[m][c][p];

                int next_t;
                next_t = assig_tasks_cores[m][c][p+1];

                if (tasks_start[t] + ATC[t][m] - tasks_start[next_t] > 1) {
                    printf("[ERROR][m=%d,c=%d] Task %d runs from %d to %.0f collides with task %d starting on %d\n",
                           m, c, t, tasks_start[t], tasks_start[t] + ATC[t][m], next_t, tasks_start[next_t]);
                }
            }

            if (num_tasks_cores[m][c] > 0) {
                int last_task;
                last_task = assig_tasks_cores[m][c][num_tasks_cores[m][c]-1];

                if (tasks_start[last_task] + ATC[last_task][m] != mach[m][c]) {
                    printf("[ERROR][m=%d,c=%d] Last task %d runs from %d to %.0f but core exec time is %.0f\n", m, c,
                           last_task, tasks_start[last_task], tasks_start[last_task] + ATC[last_task][m], mach[m][c]);
                }
            } else {
                if (mach[m][c] > 0) {
                    printf("[ERROR][m=%d,c=%d] Exec time is %.0f but no tasks are assigned\n", m, c, mach[m][c]);
                }
            }
        }
    }
}

int main(int argc, char *argv[]) {
    if (argc < 5) {
        printf("Sintaxis: %s <workload> <scenario> <num_tasks> <num_machines>\n", argv[0]);
        exit(1);
    }

    srand48(time(NULL));

    char *arch_inst, *arch_proc;
    arch_inst = argv[1];
    arch_proc = argv[2];

    int NT, NM;
    NT = atoi(argv[3]);
    NM = atoi(argv[4]);

#if DEBUG
    fprintf(stdout,"NT: %d, NM: %d, arch_ETC: %s, arch_proc: %s\n",NT,NM,arch_inst,arch_proc);
#endif

    /* **************************************************************
     * Inicialización del problema
     * ************************************************************** */

    float *E_IDLE = (float *) malloc(sizeof(float)*NM);
    if (E_IDLE == NULL) {
        fprintf(stderr,"Error in malloc for E_IDLE matrix, dimension %d\n",NM);
        exit(2);
    }

    float *E_MAX = (float *) malloc(sizeof(float)*NM);
    if (E_MAX == NULL) {
        fprintf(stderr,"Error in malloc for E_MAX matrix, dimension %d\n",NM);
        exit(2);
    }

    int *cores = (int *) malloc(sizeof(int)*NM);
    if (cores == NULL) {
        fprintf(stderr,"Error in malloc for cores matrix, dimension %d\n",NM);
        exit(2);
    }

    float *GFLOPS = (float *) malloc(sizeof(float)*NM);
    if (GFLOPS == NULL) {
        fprintf(stderr,"Error in malloc for GFLOPS matrix, dimension %d\n",NM);
        exit(2);
    }

    float **ETC = (float **) malloc(sizeof(float *)*NT);
    if (ETC == NULL) {
        fprintf(stderr,"Error in malloc for ETC matrix, dimensions %dx%d\n",NT,NM);
        exit(2);
    }

    int i,j,h,k;

    for (i=0; i<NT; i++) {
        ETC[i] = (float *) malloc(sizeof(float)*NM);
        if (ETC[i] == NULL) {
            fprintf(stderr,"Error in malloc, row %d in ETC\n",i);
            exit(2);
        }
    }

    float **ATC = (float **) malloc(sizeof(float *)*NT);
    if (ATC == NULL) {
        fprintf(stderr,"Error in malloc for ATC matrix, dimensions %dx%d\n",NT,NM);
        exit(2);
    }

    for (i=0; i<NT; i++) {
        ATC[i] = (float *) malloc(sizeof(float)*NM);
        if (ATC[i] == NULL) {
            fprintf(stderr,"Error in malloc, row %d in ATC\n",i);
            exit(2);
        }
    }

    int *arrival_time = (int *) malloc(sizeof(int) * NT);
    if (arrival_time == NULL) {
        fprintf(stderr, "Error in malloc for arrival times\n");
        exit(2);
    }

    // Read input files, store ETC matrix and proc. info
    FILE *fp;
    int err;

    if((fp=fopen(arch_proc, "r"))==NULL) {
        fprintf(stderr,"Can't read processor file: %s\n",arch_inst);
        exit(1);
    }

    for (j=0; j<NM; j++) {
        err = fscanf(fp,"%d %f %f %f\n",&cores[j],&GFLOPS[j],&E_IDLE[j],&E_MAX[j]);
        E_IDLE[j] = E_IDLE[j] / 1000;
        E_MAX[j] = E_MAX[j] / 1000;
    }

    //close(fp);

    FILE *fi;

    if((fi=fopen(arch_inst, "r"))==NULL) {
        fprintf(stderr,"Can't read instance file: %s\n",arch_inst);
        exit(1);
    }

    int arrived;
    float req_exec_time;
    float exec_time;

    // Intel Xeon E5440: cores=4, ssj_ops=150,979, E_IDLE=76.9, E_MAX=131.8
    float default_kssj = (150979.0/4.0) / 1000.0;

    for (i=0; i<NT; i++) {
        err = fscanf(fi,"%d %f %f", &arrived, &req_exec_time, &exec_time);
        arrival_time[i] = arrived;

        for (j=0; j<NM; j++) {
            ETC[i][j] = (req_exec_time * default_kssj) / ((GFLOPS[j]/cores[j]) / 1000);
            ATC[i][j] = (exec_time * default_kssj) / ((GFLOPS[j]/cores[j]) / 1000);
        }
    }

    //close(fi);

    /* **************************************************************
     * Inicialización de la solución
     * ************************************************************** */

    // Machine array, stores the MET.
    float **mach = (float **) malloc(sizeof(float *)*NM);
    if (mach == NULL) {
        fprintf(stderr,"Error in malloc (machine array), dimension %d\n",NM);
        exit(2);
    }

    // Machine array, stores the MET.
    int **mach_cores = (int **) malloc(sizeof(int *)*NM);
    if (mach_cores == NULL) {
        fprintf(stderr,"Error in malloc (machine cores array), dimension %d\n",NM);
        exit(2);
    }

    // Number of tasks assigned to each core
    int *tasks_start = (int *) malloc(sizeof(int)*NT);
    if (tasks_start == NULL) {
        fprintf(stderr,"Error in malloc (tasks_start), dimension %d\n",NT);
        exit(2);
    }

    // Number of tasks assigned to each core
    int **num_tasks_cores = (int **) malloc(sizeof(int *)*NM);
    if (num_tasks_cores == NULL) {
        fprintf(stderr,"Error in malloc (num_tasks_cores), dimension %d\n",NM);
        exit(2);
    }

    // Task pos currently being executed by each core
    int **exec_task_cores = (int **) malloc(sizeof(int *)*NM);
    if (exec_task_cores == NULL) {
        fprintf(stderr,"Error in malloc (exec_task_cores), dimension %d\n",NM);
        exit(2);
    }

    // Actual tasks assigned to each core
    int ***assig_tasks_cores = (int ***) malloc(sizeof(int **)*NM);
    if (assig_tasks_cores == NULL) {
        fprintf(stderr,"Error in malloc (assig_tasks_cores), dimension %d\n",NM);
        exit(2);
    }

    for (j=0; j<NM; j++) {
        mach[j] = (float *) malloc(sizeof(float)*cores[j]);
        mach_cores[j] = (int *) malloc(sizeof(int)*cores[j]);
        num_tasks_cores[j] = (int *) malloc(sizeof(int)*cores[j]);
        exec_task_cores[j] = (int *) malloc(sizeof(int)*cores[j]);
        assig_tasks_cores[j] = (int **) malloc(sizeof(int*)*cores[j]);

        if ((mach[j] == NULL) || (mach_cores[j]==NULL) || (num_tasks_cores[j]==NULL) ||
                (exec_task_cores[j] == NULL) || (assig_tasks_cores[j] == NULL)) {

            fprintf(stderr,"Error in malloc, row %d in mach.\n",j);
            exit(2);
        }

        for(h=0; h<cores[j]; h++) {
            mach[j][h] = 0.0;
            mach_cores[j][h] = h;

            num_tasks_cores[j][h] = 0;
            exec_task_cores[j][h] = 0;

            assig_tasks_cores[j][h] = (int *) malloc(sizeof(int)*NT);
        }
    }

    float *energy_mach = (float*) malloc(sizeof(float)*NM);
    if (energy_mach == NULL) {
        fprintf(stderr,"Error in malloc (energy_mach), dimension %d\n",NM);
        exit(2);
    }

    // Number of applications array
    int *napp = (int*) malloc(sizeof(float)*NM);
    if (napp == NULL) {
        fprintf(stderr,"Error in malloc (number of applications array), dimension %d\n",NM);
        exit(2);
    }

    for (j=0; j<NM; j++) {
        napp[j] = 0;
        energy_mach[j] = 0.0;
    }

    /* **************************************************************
     * Algoritmo de planificación
     * ************************************************************** */

    int current_processed_task = 0;

    float min_ct_task;

    int best_machine, best_mach_task;
    int best_task;

    float et, new_et;

    int current_time = 0;

    while ( current_processed_task < NT ) {
        // Processing new task
        current_time = arrival_time[current_processed_task];

#if DEBUG
        printf(">>> arrived task %d on %d\n", current_processed_task, current_time);
#endif

        // SCHEDULING
        // Select non-assigned tasks with maximun robustness radio - minimum completion time.
        best_task = -1;
        best_machine = -1;

        // Loop on tasks.
        i = current_processed_task;

        best_mach_task = -1;
        min_ct_task = FLT_MAX;

        // Loop on machines
        for (j=0; j<NM; j++) {
            // Evaluate MCT of (ti, mj)
            // mach[j][0] has the min local makespan for machine j.
            int min_load_core;
            min_load_core = mach_cores[j][0];

            if (mach[j][min_load_core] >= current_time) {
                et = mach[j][min_load_core] + ETC[i][j];
            } else {
                et = current_time + ETC[i][j];
            }

            if (et < min_ct_task) {
                min_ct_task = et;
                best_mach_task = j;
            }
        }

		best_task = i;
		best_machine = best_mach_task;

        int min_load_core;
        min_load_core = mach_cores[best_machine][0];

#if DEBUG
        printf("*** Assigning task %d to machine %d core %d\n", best_task, best_machine, min_load_core);
#endif

        // Ordered insertion.
        if (mach[best_machine][min_load_core] >= current_time) {
            new_et = mach[best_machine][min_load_core] + ETC[best_task][best_machine];
            tasks_start[best_task] = mach[best_machine][min_load_core];
        } else {
            new_et = current_time + ETC[best_task][best_machine];
            tasks_start[best_task] = current_time;
        }

        // Task is assigned to the first core in the ordered list.
        int tasks_min_load_core;
        tasks_min_load_core = num_tasks_cores[best_machine][min_load_core];
        assig_tasks_cores[best_machine][min_load_core][tasks_min_load_core] = best_task;
        num_tasks_cores[best_machine][min_load_core]++;

        mach[best_machine][min_load_core] = new_et;
        napp[best_machine]++;

		/*
        h = 1;
        while (( h < cores[best_machine] ) && ( new_et > mach[best_machine][mach_cores[best_machine][h]] )) {
            mach_cores[best_machine][h-1] = mach_cores[best_machine][h];
            mach_cores[best_machine][h] = min_load_core;

            h++;
        }
        * */

#if DEBUG
        printf("==================== TASKS SCHEDULED\n");
        for (i = 0; i < NM; i++) {
            printf(">> Machine %d\n", i);
            for (j = 0; j < cores[i]; j++) {
                printf("Core %d [Mak=%.2f]: ", j, mach[i][j]);
                for (int t = 0; t < num_tasks_cores[i][j]; t++) {
                    int current_task;
                    current_task = assig_tasks_cores[i][j][t];

                    printf(" (%d,%d,%.1f,%.1f)", current_task,
                           tasks_start[current_task],
                           tasks_start[current_task]+ETC[current_task][i],
                           tasks_start[current_task]+ATC[current_task][i]);
                }
                printf("\n");
            }
        }
        printf("Cores sorted:\n");
        for (int y = 0; y < NM; y++) {
            printf("Machine %d: ", y);
            for (int z = 0; z < cores[y]; z++) {
                printf(" %d", mach_cores[y][z]);
            }
            printf("\n");
        }
#endif

        for (i = 0; i < NM; i++) {
            for (j = 0; j < cores[i]; j++) {
                int run_next_task;

                do {
                    run_next_task = 0;

                    if (exec_task_cores[i][j] >= num_tasks_cores[i][j]) {
                        // Core was IDLE
                    } else {
                        int exec_pos;
                        exec_pos = exec_task_cores[i][j];

                        int exec_task;
                        exec_task = assig_tasks_cores[i][j][exec_pos];

                        if (tasks_start[exec_task] + ATC[exec_task][i] <= current_time) {
#if DEBUG
                            printf(">>> Finished EXECUTING: Mach %d Core %d Task %d [FROM %d TO %f]\n",
                                   i, j, exec_task, tasks_start[exec_task], tasks_start[exec_task] + ATC[exec_task][i]);
#endif

							/*if (exec_pos == 0) {
								tasks_start[exec_task] = arrival_time[exec_task];
								mach[i][j] = tasks_start[exec_task] + ATC[exec_task][i];
							} else {
								int prev_task;
								prev_task = assig_tasks_cores[i][j][exec_pos-1];

								if (arrival_time[exec_task] < tasks_start[prev_task] + ATC[prev_task][i]) {
									tasks_start[exec_task] = tasks_start[prev_task] + ATC[prev_task][i];
								} else {
									tasks_start[exec_task] = arrival_time[exec_task];
								}
							*/
							mach[i][j] = tasks_start[exec_task] + ATC[exec_task][i];
							//}

                            exec_task_cores[i][j]++;

                            // Updating the ETA of all the following tasks assigned to the current core
                            for (int next_pos = exec_task_cores[i][j]; next_pos < num_tasks_cores[i][j]; next_pos++) {
                                int next_task;
                                next_task = assig_tasks_cores[i][j][next_pos];

                                if (mach[i][j] > arrival_time[next_task]) {
									tasks_start[next_task] = mach[i][j];
                                    mach[i][j] += ETC[next_task][i];
                                } else {
									tasks_start[next_task] = arrival_time[next_task];
                                    mach[i][j] = arrival_time[next_task] + ETC[next_task][i];
                                }
                            }

                            run_next_task = 1;
                        }
                    }
                } while (run_next_task == 1);
            }
        }

        float curr_mach;
        int curr_mach_cores;

        // Updating knowledge about load
        for (i = 0; i < NM; i++) {
            // All cores are sorted according to this updated knowledge
            for (j = 0; j < cores[i]; j++) {
                curr_mach_cores = mach_cores[i][0];

                for (int sort = 1; sort < cores[i]; sort++) {
                    if (mach[i][curr_mach_cores] >= mach[i][mach_cores[i][sort]]) {
                        mach_cores[i][sort-1] = mach_cores[i][sort];
                        mach_cores[i][sort] = curr_mach_cores;
                    } else {
                        mach_cores[i][sort-1] = curr_mach_cores;
                        curr_mach_cores = mach_cores[i][sort];
                    }
                }
            }
        }

#if DEBUG
        printf("==================== TIME ELAPSED\n");
        for (i = 0; i < NM; i++) {
            printf(">> Machine %d\n", i);
            for (j = 0; j < cores[i]; j++) {
                printf("Core %d [Mak=%.2f]: ", j, mach[i][j]);
                for (int t = 0; t < num_tasks_cores[i][j]; t++) {
                    int current_task;
                    current_task = assig_tasks_cores[i][j][t];

                    printf(" (%d,%d,%.1f,%.1f)", current_task,
                           tasks_start[current_task],
                           tasks_start[current_task]+ETC[current_task][i],
                           tasks_start[current_task]+ATC[current_task][i]);
                }
                printf("\n");
            }
        }
        printf("Cores sorted:\n");
        for (int y = 0; y < NM; y++) {
            printf("Machine %d: ", y);
            for (int z = 0; z < cores[y]; z++) {
                printf(" %d", mach_cores[y][z]);
            }
            printf("\n");
        }
#endif

        // NEXT TASK!
        current_processed_task++;
    }

    // Finishing currently executing tasks
    for (i = 0; i < NM; i++) {
        for (j = 0; j < cores[i]; j++) {
			int run_next;

			do {
				run_next = 0;

				if (exec_task_cores[i][j] >= num_tasks_cores[i][j]) {
					// Core was IDLE
				} else {
					int exec_pos;
					exec_pos = exec_task_cores[i][j];

					int exec_task;
					exec_task = assig_tasks_cores[i][j][exec_pos];

					#if DEBUG
						printf(">>> Finished EXECUTING: Mach %d Core %d Task %d [FROM %d TO %f]\n",
							i, j, exec_task, tasks_start[exec_task], tasks_start[exec_task] + ATC[exec_task][i]);
					#endif

					if (exec_pos == 0) {
						tasks_start[exec_task] = arrival_time[exec_task];
						mach[i][j] = tasks_start[exec_task] + ATC[exec_task][i];
					} else {
						int prev_task;
						prev_task = assig_tasks_cores[i][j][exec_pos-1];

						if (arrival_time[exec_task] < tasks_start[prev_task] + ATC[prev_task][i]) {
							tasks_start[exec_task] = tasks_start[prev_task] + ATC[prev_task][i];
						} else {
							tasks_start[exec_task] = arrival_time[exec_task];
						}

						mach[i][j] = tasks_start[exec_task] + ATC[exec_task][i];
					}

					exec_task_cores[i][j]++;

					run_next = 1;
				}
			} while (run_next == 1);
        }
    }

    float curr_mach;
    int curr_mach_cores;

    // Updating knowledge about load
    for (i = 0; i < NM; i++) {
        // All cores are sorted according to this updated knowledge
        for (j = 0; j < cores[i]; j++) {
            curr_mach_cores = mach_cores[i][0];

            for (int sort = 1; sort < cores[i]; sort++) {
                if (mach[i][curr_mach_cores] >= mach[i][mach_cores[i][sort]]) {
                    mach_cores[i][sort-1] = mach_cores[i][sort];
                    mach_cores[i][sort] = curr_mach_cores;
                } else {
                    mach_cores[i][sort-1] = curr_mach_cores;
                    curr_mach_cores = mach_cores[i][sort];
                }
            }
        }
    }

    float makespan = 0.0;

    for (i = 0; i < NM; i++) {
#if DEBUG || SHOW_SOLUTION
        printf("MACHINE %d: ", i);
        for (j = 0; j < cores[i]; j++) {
            printf(" %d|%.2f", j, mach[i][j]);
        }
        printf("\n   ORDER: ");
        for (j = 0; j < cores[i]; j++) {
            printf(" %d", mach_cores[i][j]);
        }
        printf("\n");
#endif

        int loaded_core;
        loaded_core = mach_cores[i][cores[i]-1];

#if DEBUG || SHOW_SOLUTION
        printf("Most loaded core for %d: %d\n", i, loaded_core);
#endif

        int current_task;
        current_task = assig_tasks_cores[i][loaded_core][num_tasks_cores[i][loaded_core]-1];

#if DEBUG || SHOW_SOLUTION
        printf("Machine %d => Task %d on %.2f\n",
               i, current_task, tasks_start[current_task]+ATC[current_task][i]);
#endif

        if (tasks_start[current_task]+ATC[current_task][i] > makespan) {
            makespan = tasks_start[current_task]+ATC[current_task][i];
        }
    }

#if DEBUG || SHOW_SOLUTION
    for (i = 0; i < NM; i++) {
        printf(">> Machine %d\n", i);
        for (j = 0; j < cores[i]; j++) {
            printf("Core %d: ", j);
            for (int t = 0; t < num_tasks_cores[i][j]; t++) {
                int current_task;
                current_task = assig_tasks_cores[i][j][t];

                printf(" (%d,%d,%.2f)", current_task,
                       tasks_start[current_task], tasks_start[current_task]+ATC[current_task][i]);
            }
            printf("\n");
        }
    }
#endif

    float energy_consumption = 0.0;

    for (i = 0; i < NM; i++) {
        float idle_time, computing_time;
        idle_time = 0.0;
        computing_time = 0.0;

        for (j = 0; j < cores[i]; j++) {
            for (int t = 0; t < num_tasks_cores[i][j]; t++) {
                int current_task, prev_task;
                current_task = assig_tasks_cores[i][j][t];

                if (t == 0) {
                    if (tasks_start[current_task] > 0) {
                        idle_time += tasks_start[current_task];
                    }
                } else {
                    prev_task = assig_tasks_cores[i][j][t-1];

                    if (tasks_start[current_task] > tasks_start[prev_task]+ATC[prev_task][i]) {
                        idle_time += tasks_start[current_task] - tasks_start[prev_task] + ATC[prev_task][i];
                    }
                }

                computing_time += ATC[current_task][i];
            }

            if (num_tasks_cores[i][j] > 0) {
                int last_task;
                last_task = num_tasks_cores[i][j]-1;

                idle_time += makespan - tasks_start[last_task] + ATC[last_task][i];
            } else {
                idle_time += makespan;
            }
        }

        //energy_consumption += makespan * E_IDLE[i] + computing_time * (E_MAX[i] - E_IDLE[i]) / cores[i];
        
        energy_consumption += makespan * E_IDLE[i];
        
        float aux_energy, rand_energy_error;
        
        aux_energy = computing_time * (E_MAX[i] - E_IDLE[i]) / cores[i];
        rand_energy_error = (drand48() * 10 - 5) / 100;
        
        energy_consumption += aux_energy + aux_energy * rand_energy_error; 
    }

#if DEBUG || SHOW_SOLUTION
    printf("MAKESPAN:%.2f\n", makespan);
    printf("ENERGY  :%.2f\n", energy_consumption);
#endif

    fprintf(stdout,"%f %f\n",makespan,energy_consumption);

#if DO_VALIDATE
    validate_solution(NT, NM, E_IDLE, E_MAX, cores, ETC, ATC, arrival_time,
                      mach, mach_cores, tasks_start, num_tasks_cores, exec_task_cores, assig_tasks_cores);
#endif
}
