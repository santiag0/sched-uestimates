// Min-MIN energy-aware scheduler.
// Phase 1: Pair with minimum energy consumption
// Phase 2: Minimum ETC.
// Parameters : <instance_ETC_file> <num_tasks> <num_machines>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <math.h>
#include <time.h>

#define NO_ASIG -1
#define SIZE_NOM_ARCH 180

#define DEBUG 0

float energy(int i, int j, float *mach, int num_cores, float E_I, float E_M, float new_et, int napp) {
// Calculates the energy when assigning task i to machine j.

    float curr_energy = 0.0;
    int h;

    float* mach_copy = (float *) malloc(sizeof(float)*num_cores);

#if DEBUG
    printf("\n----\nfunction energy (init): napp: %d, num_cores: %d, E_I: %f, E_M: %f, curr energy: %f\n",napp,num_cores,E_I,E_M,curr_energy);
    printf("mach_copy: [");
#endif

    for(h=0; h<num_cores; h++) {
        mach_copy[h] = mach[h];
#if DEBUG
        printf("%f ",mach_copy[h]);
#endif
    }
#if DEBUG
    printf("] (machine %d, %d cores)\n",j,num_cores);
#endif

// Ordered insertion

    h = 1;
    if ( new_et < mach_copy[h]) {
        h = 0;
    } else {
        while((h<num_cores) && (new_et > mach[h])) {
            mach_copy[h-1] = mach_copy[h];
            h++;
        }
        //if (h == num_cores){
        h--;
        //}
    }
    mach_copy[h] = new_et;
    napp++;

#if DEBUG
    printf("new_et: %f\n insert in pos %d\n",new_et,h);
    printf("mach_copy: [");
    for(h=0; h<num_cores; h++) {
        printf("%f ",mach_copy[h]);
    }
    printf("]\n");
#endif

    float E_h;

// Calculate energy
    if (napp >= num_cores) {
        curr_energy = E_M*mach_copy[0];
#if DEBUG
        printf("napp >= ncores. napp: %d, num_cores: %d, E_I: %f, E_M: %f, mach_copy[0]=%f, curr energy: %f\n",napp,num_cores,E_I,E_M,mach_copy[0],curr_energy);
#endif
        for(h=1; h<num_cores; h++) {
            E_h = E_I+((E_M-E_I)*(num_cores-h)/num_cores);
            curr_energy = curr_energy + E_h*(mach_copy[h]-mach_copy[h-1]);
#if DEBUG
            printf("for (1): napp: %d, num_cores: %d, E_I: %f, E_M: %f, E(%d): %f, mach_copy[%d]=%f, mach_copy[%d]=%f, curr energy: %f\n",napp,num_cores,E_I,E_M,num_cores-h,E_h,h,mach_copy[h],h-1,mach_copy[h-1],curr_energy);
#endif
        }
    } else {
        E_h = E_I+((E_M-E_I)*napp/num_cores);
        curr_energy = E_h * mach_copy[num_cores-napp];
#if DEBUG
        printf("napp < ncores. napp: %d, num_cores: %d, E_I: %f, E_M: %f, E(%d): %f, mach_copy[%d]=%f, mach_copy[%d]=%f, curr energy: %f\n",napp,num_cores,E_I,E_M,napp,E_h,h,mach_copy[h],h-1,mach_copy[h-1],curr_energy);
#endif
        for(h=1; h<napp; h++) {
            curr_energy = curr_energy + (mach_copy[num_cores-napp+h]-mach_copy[num_cores-napp+h-1])*(E_I+((E_M-E_I)*(napp-h)/num_cores));
#if DEBUG
            printf("for: napp: %d, num_cores: %d, E_I: %f, E_M: %f, mach_copy[%d]=%f, mach_copy[%d]=%f, curr energy: %f\n",napp,num_cores,E_I,E_M,h,mach_copy[h],h-1,mach_copy[h-1],curr_energy);
#endif
        }
    }

#if DEBUG
    printf("napp: %d, num_cores: %d, E_I: %f, E_M: %f, energy: %f\n",napp,num_cores,E_I,E_M,curr_energy);
    printf("function energy end\n--------\n");
#endif

    free(mach_copy);

    return curr_energy;
}

int main(int argc, char *argv[]) {
    if (argc < 5) {
        printf("Sintaxis: %s <workload> <scenario> <num_tasks> <num_machines>\n", argv[0]);
        exit(1);
    }

    srand48(time(NULL));

    char *arch_inst, *arch_proc;
    arch_inst = argv[1];
    arch_proc = argv[2];

    int NT, NM;
    NT = atoi(argv[3]);
    NM = atoi(argv[4]);

#if DEBUG
    fprintf(stdout,"NT: %d, NM: %d, arch_ETC: %s, arch_proc: %s\n",NT,NM,arch_inst,arch_proc);
#endif

    float *E_IDLE = (float *) malloc(sizeof(float)*NM);
    if (E_IDLE == NULL) {
        fprintf(stderr,"Error in malloc for E_IDLE matrix, dimension %d\n",NM);
        exit(2);
    }

    float *E_MAX = (float *) malloc(sizeof(float)*NM);
    if (E_MAX == NULL) {
        fprintf(stderr,"Error in malloc for E_MAX matrix, dimension %d\n",NM);
        exit(2);
    }

    int *cores = (int *) malloc(sizeof(int)*NM);
    if (cores == NULL) {
        fprintf(stderr,"Error in malloc for cores matrix, dimension %d\n",NM);
        exit(2);
    }

    float *GFLOPS = (float *) malloc(sizeof(float)*NM);
    if (GFLOPS == NULL) {
        fprintf(stderr,"Error in malloc for GFLOPS matrix, dimension %d\n",NM);
        exit(2);
    }

    float **ETC = (float **) malloc(sizeof(float *)*NT);
    if (ETC == NULL) {
        fprintf(stderr,"Error in malloc for ETC matrix, dimensions %dx%d\n",NT,NM);
        exit(2);
    }

    int i,j,h,k;

    for (i=0; i<NT; i++) {
        ETC[i] = (float *) malloc(sizeof(float)*NM);
        if (ETC[i] == NULL) {
            fprintf(stderr,"Error in malloc, row %d in ETC\n",i);
            exit(2);
        }
    }

    float **ATC = (float **) malloc(sizeof(float *)*NT);
    if (ATC == NULL) {
        fprintf(stderr,"Error in malloc for ATC matrix, dimensions %dx%d\n",NT,NM);
        exit(2);
    }

    for (i=0; i<NT; i++) {
        ATC[i] = (float *) malloc(sizeof(float)*NM);
        if (ATC[i] == NULL) {
            fprintf(stderr,"Error in malloc, row %d in ATC\n",i);
            exit(2);
        }
    }

    // Machine array, stores the MET.
    float **mach = (float **) malloc(sizeof(float *)*NM);
    if (mach == NULL) {
        fprintf(stderr,"Error in malloc (machine array), dimension %d\n",NM);
        exit(2);
    }

    // Machine array, stores the MET.
    float **mach_true = (float **) malloc(sizeof(float *)*NM);
    if (mach_true == NULL) {
        fprintf(stderr,"Error in malloc (machine true array), dimension %d\n",NM);
        exit(2);
    }

    // Machine array, stores the MET.
    int **mach_cores = (int **) malloc(sizeof(int *)*NM);
    if (mach_cores == NULL) {
        fprintf(stderr,"Error in malloc (machine cores array), dimension %d\n",NM);
        exit(2);
    }

    int *arrival_time = (int *) malloc(sizeof(int) * NT);
    if (arrival_time == NULL) {
        fprintf(stderr, "Error in malloc for arrival times\n");
        exit(2);
    }

    // Read input files, store ETC matrix and proc. info
    FILE *fp;

    int err;

    if((fp=fopen(arch_proc, "r"))==NULL) {
        fprintf(stderr,"Can't read processor file: %s\n",arch_inst);
        exit(1);
    }

    for (j=0; j<NM; j++) {
        err = fscanf(fp,"%d %f %f %f\n",&cores[j],&GFLOPS[j],&E_IDLE[j],&E_MAX[j]);
        E_IDLE[j] = E_IDLE[j] / 1000;
        E_MAX[j] = E_MAX[j] / 1000;
    }

    for (j=0; j<NM; j++) {
        mach[j] = (float *) malloc(sizeof(float)*cores[j]);
        mach_true[j] = (float *) malloc(sizeof(float)*cores[j]);
        mach_cores[j] = (int *) malloc(sizeof(int)*cores[j]);

        if ((mach[j] == NULL)||(mach_true[j] == NULL)||(mach_cores[j]==NULL)) {
            fprintf(stderr,"Error in malloc, row %d in mach.\n",j);
            exit(2);
        }
        for(h=0; h<cores[j]; h++) {
            mach[j][h] = 0.0;
            mach_true[j][h] = 0.0;
            mach_cores[j][h] = h;
        }
    }

    close(fp);

    FILE *fi;

    if((fi=fopen(arch_inst, "r"))==NULL) {
        fprintf(stderr,"Can't read instance file: %s\n",arch_inst);
        exit(1);
    }

    float req_exec_time;
    float exec_time;
    int arrived;

    // Intel Xeon E5440: cores=4, ssj_ops=150,979, E_IDLE=76.9, E_MAX=131.8
    float default_kssj = (150979.0/4.0) / 1000.0;

    for (i=0; i<NT; i++) {
        err = fscanf(fi,"%d %f %f", &arrived, &req_exec_time, &exec_time);
        arrival_time[i] = arrived;

        for (j=0; j<NM; j++) {
            ETC[i][j] = (req_exec_time * default_kssj) / ((GFLOPS[j]/cores[j]) / 1000);
            ATC[i][j] = (exec_time * default_kssj) / ((GFLOPS[j]/cores[j]) / 1000);
        }
    }

    close(fi);

    float *energy_mach = (float*) malloc(sizeof(float)*NM);
    if (energy_mach == NULL) {
        fprintf(stderr,"Error in malloc (energy_mach), dimension %d\n",NM);
        exit(2);
    }

    // Number of applications array
    int *napp = (int*) malloc(sizeof(float)*NM);
    if (napp == NULL) {
        fprintf(stderr,"Error in malloc (number of applications array), dimension %d\n",NM);
        exit(2);
    }

    for (j=0; j<NM; j++) {
        napp[j] = 0;
        energy_mach[j] = 0.0;
    }

    // Assigned tasks array
    int *asig= (int*) malloc(sizeof(float)*NT);
    if (asig == NULL) {
        fprintf(stderr,"Error in malloc (assigned tasks array), dimension %d\n",NT);
        exit(2);
    }

    int nro_asig = 0;
    for (i=0; i<NT; i++) {
        asig[i]=NO_ASIG;
    }

    // Assigned core array
    int *asig_core= (int*) malloc(sizeof(float)*NT);
    if (asig_core == NULL) {
        fprintf(stderr,"Error in malloc (assigned core array), dimension %d\n",NT);
        exit(2);
    }

    for (i=0; i<NT; i++) {
        asig_core[i]=NO_ASIG;
    }

    float mct_i_j;
    float max_ct;

    int best_machine, best_mach_task;
    int best_task;

    float ct, delta_energy, min_energy, best_delta_energy, et, new_et;

    float mak_temp = 0.0;

    while (nro_asig < NT) {
        // Select non-assigned tasks with maximun robustness radio - minimum completion time.
        best_task = -1;
        best_machine = -1;
        max_ct = 0.0;

        // Loop on tasks.
        for (i=0; i<NT; i++) {
            best_mach_task = -1;
            min_energy = FLT_MAX;

            if (asig[i] == NO_ASIG) {
                // Loop on machines
                for (j=0; j<NM; j++) {
                    // Evaluate delta energy of (ti, mj)
                    // mach[j][0] has the min local makespan for machine j.

                    if (mach[j][0] >= arrival_time[i]) {
                        et = mach[j][0]+ETC[i][j];
                    } else {
                        et = arrival_time[i]+ETC[i][j];
                    }

                    delta_energy = energy(i,j,mach[j],cores[j],E_IDLE[j],E_MAX[j],et,napp[j]) - energy_mach[j];
#if DEBUG
                    printf("energy_mach[%d]: %f, delta energy (%d,%d):%f -",j,energy_mach[j],i,j,delta_energy);
#endif
                    if (delta_energy < min_energy) {
                        min_energy = delta_energy;
                        best_mach_task = j;
#if DEBUG
                        printf("delta energy best pair(%d,%d): %f\n",i,best_mach_task,delta_energy);
#endif
                    }
                }

                mct_i_j = mach[best_mach_task][0]+ETC[i][best_mach_task];

                if (mct_i_j > max_ct) {
                    max_ct = mct_i_j;
                    best_task = i;
                    best_machine = best_mach_task;
                    best_delta_energy = min_energy;
#if DEBUG
                    printf("max_ct (%d,%d): %f, best_delta_energy: %f\n",best_task,best_machine,mct_i_j,best_delta_energy);
#endif
                }
            }
        }

#if DEBUG
        printf("********************* Assign task %d to machine %d\n",best_task,best_machine);
#endif

        // Ordered insertion.
        if (mach[best_machine][0] >= arrival_time[i]) {
            new_et = mach[best_machine][0]+ETC[best_task][best_machine];
        } else {
            new_et = arrival_time[i]+ETC[best_task][best_machine];
        }

        float new_at;
        if (mach_true[best_machine][0] >= arrival_time[i]) {
            new_at = mach_true[best_machine][0]+ATC[best_task][best_machine];
        } else {
            new_at = arrival_time[i]+ATC[best_task][best_machine];
        }

        // Task is assigned to the first core in the ordered list.
        asig_core[best_task] = mach_cores[best_machine][0];

#if DEBUG
        printf("new_et: %f\n",new_et);
        printf("mach[%d]: [",best_machine);
        for(k=0; k<cores[best_machine]; k++) {
            printf("%f ",mach[best_machine][k]);
        }
        printf("] (SHIFT) -> ");
#endif

        h = 1;
        if ( new_et < mach[best_machine][h]) {
            h = 0;
        } else {
            while((h<cores[best_machine]) && (new_et > mach[best_machine][h])) {
                mach[best_machine][h-1] = mach[best_machine][h];

                mach_true[best_machine][h-1] = mach_true[best_machine][h];
                mach_cores[best_machine][h-1] = mach_cores[best_machine][h];

                if (h<cores[best_machine]) {
                    h++;
                }
            }
            //if (h == cores[best_machine]){
            h--;
            //}
        }

#if DEBUG
        printf("mach[%d]: [",best_machine);
        for(k=0; k<cores[best_machine]; k++) {
            printf("%f ",mach[best_machine][k]);
        }
        printf("] -> ETC[%d,%d]=%f, inserto en pos %d valor %f\n",best_task,best_machine,ETC[best_task][best_machine],h,new_et);
#endif

        mach[best_machine][h] = new_et;
        mach_true[best_machine][h] = new_at;
        mach_cores[best_machine][h] = asig_core[best_task];

#if DEBUG
        printf("mach[%d]: [",best_machine);
        for(k=0; k<cores[best_machine]; k++) {
            printf("%f ",mach[best_machine][k]);
        }
#endif

        asig[best_task]=best_machine;
        energy_mach[best_machine]+=best_delta_energy;
        napp[best_machine]++;
        nro_asig++;

#if DEBUG
        printf("] napp: %d, energy: %f\n***************************\n",napp[best_machine],energy_mach[best_machine]);
#endif

        /* Update energy */
        float delta_mak = 0.0;

        if (mach[best_machine][cores[best_machine]-1] > mak_temp) {
            delta_mak = mach[best_machine][cores[best_machine]-1] - mak_temp;
#if DEBUG
            printf("Delta mak: %f\n",delta_mak);
            printf("Update mak_temp: %f ",mak_temp);
#endif
            mak_temp = mach[best_machine][cores[best_machine]-1];
#if DEBUG
            printf("-> %f\n",mak_temp);
#endif
            for (j=0; j<NM; j++) {
#if DEBUG
                printf("Update energy[%d]:%f ",j,energy_mach[j]);
#endif
                //energy_mach[j]=energy_mach[j]+E_IDLE[j]*(mak_temp-mach[j][cores[j]-1]);
                if ( j != best_machine ) {
                    energy_mach[j] = energy_mach[j]+E_IDLE[j] * delta_mak;
                }
#if DEBUG
                printf("%f\n",j,energy_mach[j]);
#endif
            }
        }

    }

    float makespan = 0.0;
    int heavy = -1;
    float mak_local = 0.0;
    float total_energy = 0.0;

    for (j=0; j<NM; j++) {
        mak_local = mach[j][cores[j]-1];
        if (mak_local > makespan) {
            makespan = mak_local;
            heavy = j;
        }
        total_energy += energy_mach[j];

#if DEBUG
        printf("M[%d]: mak_local %f energy %f, total energy %f (%d tasks). %d cores, %.2f GFLOPS, E:%.2f-%.2f\n",j,mak_local,energy_mach[j],total_energy,napp[j],cores[j],GFLOPS[j],E_IDLE[j],E_MAX[j]);
#endif
    }

#if DEBUG
    fprintf(stdout,"[");
    for (i=0; i<NT; i++) {
        fprintf(stdout,"%d ",asig[i]);
    }
    fprintf(stdout,"]\n");
#endif

#if DEBUG
    fprintf(stdout,"EXPECTED makespan: %f energy consumption: %f heavy: %d\n",makespan,total_energy,heavy);
#else
    //fprintf(stdout,"%f %f\n",makespan,total_energy);
#endif

    /* === now to compute the actual makespan and energy consumption === */
    float actual_makespan = 0.0;
    float actual_total_energy = 0.0;

    for (j=0; j<NM; j++) {
        mak_local = 0.0;

        for (i=0; i<cores[j]; i++) {
            if (mak_local < mach_true[j][i]) {
                mak_local = mach_true[j][i];
            }
        }

        if (mak_local > actual_makespan) {
            actual_makespan = mak_local;
            heavy = j;
        }
    }

    float actual_local_energy;
    float aux, rand_energy_error;

    for (j=0; j<NM; j++) {
        mak_local = 0.0;
        actual_local_energy = E_IDLE[j] * actual_makespan;

        for (i=0; i<cores[j]; i++) {
            aux = mach_true[j][i] * ((E_MAX[j] - E_IDLE[j])/cores[j]);
            rand_energy_error = (drand48() * 10 - 5) / 100;
            actual_local_energy += aux + aux * rand_energy_error;
        }

        actual_total_energy += actual_local_energy;

#if DEBUG
        printf("M[%d]: mak_local %f actual local energy %f, actual total energy %f (%d tasks). %d cores, %.2f GFLOPS, E:%.2f-%.2f\n",j,mak_local,actual_local_energy,actual_total_energy,napp[j],cores[j],GFLOPS[j],E_IDLE[j],E_MAX[j]);
#endif
    }

#if DEBUG
    fprintf(stdout,"ACTUAL makespan: %f energy consumption: %f heavy: %d\n",actual_makespan,actual_total_energy,heavy);
#else
    //fprintf(stdout,"%f %f\n",actual_makespan,actual_total_energy);
#endif

    fprintf(stdout,"%f %f %f %f\n", makespan, actual_makespan, total_energy, actual_total_energy);

    /*
    for (i=0; i<NT; i++) {
        fprintf(stderr,"%d ",asig[i]);
    }
    fprintf(stderr, "\n");
    for (i=0; i<NT; i++) {
        fprintf(stderr,"%d ",asig_core[i]);
    }
    fprintf(stderr, "\n");
    * */
}
