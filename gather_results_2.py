#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  gather_results.py
#
#  Copyright 2014 santiago <santiago@MARVIN>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import sys
import io
import numpy

MAX_SCENARIOS=400
MAX_WORKLOADS=50

ALGORITHMS=("MaxMin_min", "MaxMIN", "SuffMIN", "MaxMin_min_rs", "MaxMIN_rs", "SuffMIN_rs", "MIN_d", "Min_min_d")

ERROR_LEVEL=("l","m","h","n")
PARALLELISM_LEVEL=("l",) #("l","m","h")
ARRIVAL_LEVEL=("m","h")

SCENARIO_HETER = ("l", "h")

#sol_${ALG_NAME}_${TASK_NUM}_${ERROR_LEVEL}_${PARALLELISM_LEVEL}_${ARRIVAL_LEVEL}_${NUM_LOAD}__${HETER_LEVEL}_${NUM_SCENARIO}

def main():
    if len(sys.argv) != 3:
        print("Error: {0} <num_tasks> <num_machines>".format(sys.argv[0]))
        exit(-1)

    num_tasks = int(sys.argv[1])
    num_machines = int(sys.argv[2])

    MAKESPAN_IMPROV = {}
    ENERGY_IMPROV = {}

    for s_heter in SCENARIO_HETER:
        MAKESPAN_IMPROV[s_heter] = {}
        ENERGY_IMPROV[s_heter] = {}

        s_num = 0

        for w_para in PARALLELISM_LEVEL:
            for w_error in ERROR_LEVEL:
                MAKESPAN_IMPROV[s_heter][w_error] = {}
                ENERGY_IMPROV[s_heter][w_error] = {}

                for w_arr in ARRIVAL_LEVEL:
                    MAKESPAN_IMPROV[s_heter][w_error][w_arr] = {}
                    ENERGY_IMPROV[s_heter][w_error][w_arr] = {}

                    for alg in ALGORITHMS:
                        MAKESPAN_IMPROV[s_heter][w_error][w_arr][alg] = []
                        ENERGY_IMPROV[s_heter][w_error][w_arr][alg] = []

                    for w_num in range(MAX_WORKLOADS):
                        MAKESPAN_MAX = 0.0
                        ENERGY_MAX = 0.0

                        MAKESPAN_VALUES = {}
                        ENERGY_VALUES = {}

                        for alg in ALGORITHMS:
                            BASE_PATH = alg + "_" + str(num_tasks) + "_" + str(num_machines)
                            FILE_PATH = "sol_" + alg + "_" + str(num_tasks) + "_" + w_error + "_" + w_para + "_" + w_arr + "_" + str(w_num) + "__" + s_heter + "_" + str(s_num)

                            #print(BASE_PATH + "/" + FILE_PATH)

                            with open(BASE_PATH + "/" + FILE_PATH) as f:
                                content_raw = f.readline()
                                values = content_raw.strip().split(" ")

                                if (len(values) == 2):
                                    a_makespan = float(values[0].strip())
                                    a_energy = float(values[1].strip())
                                else:
                                    e_makespan = float(values[0].strip())
                                    a_makespan = float(values[1].strip())
                                    e_energy = float(values[2].strip())
                                    a_energy = float(values[3].strip())

                                MAKESPAN_VALUES[alg] = a_makespan
                                ENERGY_VALUES[alg] = a_energy

                                if (a_makespan > MAKESPAN_MAX): MAKESPAN_MAX = a_makespan
                                if (a_energy > ENERGY_MAX): ENERGY_MAX = a_energy

                        for alg in ALGORITHMS:
                            if (MAKESPAN_VALUES[alg] == MAKESPAN_MAX):
                                MAKESPAN_IMPROV[s_heter][w_error][w_arr][alg].append(0.0)
                            else:
                                MAKESPAN_IMPROV[s_heter][w_error][w_arr][alg].append((MAKESPAN_MAX-MAKESPAN_VALUES[alg])/MAKESPAN_MAX)

                            if (ENERGY_VALUES[alg] == ENERGY_MAX):
                                ENERGY_IMPROV[s_heter][w_error][w_arr][alg].append(0.0)
                            else:
                                ENERGY_IMPROV[s_heter][w_error][w_arr][alg].append((ENERGY_MAX-ENERGY_VALUES[alg])/ENERGY_MAX)

                        s_num = s_num + 1

    for w_error in ERROR_LEVEL:
        print("=== ERROR {0} =================================".format(w_error))

        print("heter\terror\tarrival\t", end="")
        for alg in ALGORITHMS:
            print("{0}\t".format(alg), end="")
        print("")

        for s_heter in SCENARIO_HETER:
            for w_para in PARALLELISM_LEVEL:
                for w_arr in ARRIVAL_LEVEL:
                    print("{0}\t{1}\t{2}\t".format(s_heter, w_error, w_arr), end="")

                    for alg in ALGORITHMS:
                        mean_makespan = numpy.mean(MAKESPAN_IMPROV[s_heter][w_error][w_arr][alg])
                        mean_energy = numpy.mean(ENERGY_IMPROV[s_heter][w_error][w_arr][alg])

                        print("{0:4.1f} {1:4.1f}\t".format(mean_makespan*100, mean_energy*100), end="")

                    print("")

        print("\t\tTOTAL\t", end="")
        for alg in ALGORITHMS:
            sum_makespan = 0.0
            sum_energy = 0.0
            count = 0

            for s_heter in SCENARIO_HETER:
                for w_para in PARALLELISM_LEVEL:
                    for w_arr in ARRIVAL_LEVEL:
                        count = count + len(MAKESPAN_IMPROV[s_heter][w_error][w_arr][alg])
                        sum_makespan = sum_makespan + numpy.sum(MAKESPAN_IMPROV[s_heter][w_error][w_arr][alg])
                        sum_energy = sum_energy + numpy.sum(ENERGY_IMPROV[s_heter][w_error][w_arr][alg])

            print("{0:4.1f} {1:4.1f}\t".format((sum_makespan/count)*100, (sum_energy/count)*100), end="")
        print("")

    return 0

if __name__ == '__main__':
    main()

