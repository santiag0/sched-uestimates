TASKS[0]=1024
TASKS[1]=2048
TASKS[2]=4096

MACHINES[0]=8
MACHINES[1]=12
MACHINES[2]=16

ALGS[0]=MaxMIN
ALGS[1]=MaxMin_min
ALGS[2]=SuffMIN
ALGS[3]=MaxMIN_rs
ALGS[4]=MaxMin_min_rs
ALGS[5]=SuffMIN_rs
ALGS[6]=Min_min_d
ALGS[7]=MIN_d

for (( i=0;i<1;i++ )) {
	for (( j=0;j<3;j++ )) {
		for (( k=0;k<8;k++ )) {
			qsub qsub_${ALGS[k]}_${TASKS[i]}_${MACHINES[j]}.sh
		}
	}
}
