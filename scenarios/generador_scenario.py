#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2013 Santiago Iturriaga <santiago@marga>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import sys
import random
import numpy

HETER_LEVEL = (0.025, 0.25)

def main(list_proc, machine_count, scenario_count, heter_arg):
    machines_list = []
    scenario_number = 0
    
    if heter_arg == 'l':
        h_level = HETER_LEVEL[0]
    elif heter_arg == 'h':
        h_level = HETER_LEVEL[1]
    else:
        print("ERROR!")
        exit(-1)
    
    with open(list_proc) as machines_file:
        for m in machines_file:
            if m.strip():
                machines_list.append(m)
    
    std_dev = len(machines_list) * h_level
    range_min = int(numpy.floor(std_dev))
    range_max = int(numpy.ceil(len(machines_list) - std_dev))
    
    #print("std_dev={0:.2f} min={1:.2f} max={2:.2f}".format(std_dev,range_min,range_max))
    
    for s in range(scenario_count):
        ssj_values = []
        cores_values = []
        selected_machines = []
        
        centre_idx = numpy.random.randint(range_min, range_max)
        
        #print("====== centre_idx={0}".format(centre_idx))
        
        for i in range(machine_count):           
            m_idx = int(round(numpy.random.normal(centre_idx, std_dev)))
            if m_idx < 0: m_idx = 0
            if m_idx >= len(machines_list): m_idx = len(machines_list)-1
            #print("m_idx={0}".format(m_idx))
            
            m = machines_list[m_idx]
            
            m_data = m.split('\t')
            ssj_values.append(float(m_data[1].strip()) / int(m_data[0].strip()))
            cores_values.append(int(m_data[0].strip()))
            
            selected_machines.append(m)
        
        min_ssj = min(ssj_values)
        max_ssj = max(ssj_values)

        max_cores = max(cores_values)
        total_cores = sum(cores_values)
        
        filename = "s." + heter_arg + "." + str(scenario_number)
        scenario_number = scenario_number + 1
        
        print(filename)
        
        with open(filename, 'w') as output:
            for m in selected_machines:
                output.write(m)
    
    return 0

if __name__ == '__main__':
    if len(sys.argv) != 6:
        print("Error!")
        print("Usage: python {0} <list_proc> <#machines> <#scenarios> <h_level> <seed>".format(sys.argv[0]))
        sys.exit(-1)
        
    print("List processors: {0}".format(sys.argv[1]))
    list_proc = sys.argv[1]
        
    print("# machines: {0}".format(sys.argv[2]))
    machine_count = int(sys.argv[2])

    print("# scenarios: {0}".format(sys.argv[3]))
    scenario_count = int(sys.argv[3])

    print("heterogeneity level: {0}".format(sys.argv[4]))
    heter_arg = sys.argv[4].strip().lower()
    
    print("seed: {0}".format(sys.argv[5]))
    seed = int(sys.argv[5])
    
    numpy.random.seed(seed)
    main(list_proc, machine_count, scenario_count, heter_arg)
