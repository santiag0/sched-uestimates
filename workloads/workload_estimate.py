#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  workload_deadline.py
#
#  Copyright 2014 santiago <santiago@MARVIN>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import sys
import numpy

DEBUG = 0

ERROR_HIGH_SERIAL = ((0.25,0.35),(0.15,0.70),(0.15,0.95),(0.40,1.00))
ERROR_MED_SERIAL = ((0.35,0.35),(0.25,0.70),(0.10,0.95),(0.30,1.00))
ERROR_LOW_SERIAL = ((0.45,0.35),(0.35,0.70),(0.05,0.95),(0.20,1.00))
ERROR_NONE_SERIAL = ((1.00,0),(1.00,0),(1.00,0),(1.00,0))

ERROR_HIGH_PARALLEL  = ((0.05,0.35),(0.10,0.70),(0.22,0.95),(0.63,1.00))
ERROR_MED_PARALLEL  = ((0.15,0.35),(0.15,0.70),(0.17,0.95),(0.53,1.00))
ERROR_LOW_PARALLEL = ((0.25,0.35),(0.20,0.70),(0.12,0.95),(0.43,1.00))
ERROR_NONE_PARALLEL = ((1.00,0),(1.00,0),(1.00,0),(1.00,0))

ETIME_SERIAL   = ((0.50,0.2),(0.02,0.4),(0.2,0.6),(0.01,0.8),(0.45,1))
ETIME_PARALLEL = ((0.60,0.2),(0.07,0.4),(0.03,0.6),(0.03,0.8),(0.27,1))

#CHANCE_SERIAL = (0.75, 0.55, 0.35)
CHANCE_SERIAL = (1, )

MAX_CPU = 64

# Todas las unidades de tiempo son segundos.
#EXEC_MIN = 60        # 1 minuto.
EXEC_MAX = 100800	  # 28 horas.

ARRIVAL_RATE = (0.158, 0.317, 0.634, None)

def generate_task(current_time, t_cpu, etime_array, error_array):
    index = 0
    etime_ubound = etime_array[index][0]
    etime_range = numpy.random.random()

    while etime_ubound < etime_range and index < 4:
        index = index + 1
        etime_ubound = etime_ubound + etime_array[index][0]

    if index == 0: etime_rel = numpy.random.uniform(0.05, etime_array[index][1])
    else: etime_rel = numpy.random.uniform(etime_array[index-1][1], etime_array[index][1])

    if DEBUG: print("etime_range=", etime_range, "etime_rel=", etime_rel)

    t_etime = etime_rel * EXEC_MAX

    if DEBUG: print("t_etime=", t_etime)

    t_error_range = numpy.random.random()

    index = 0
    error_ubound = error_array[index][0]
    error_range = numpy.random.random()

    while error_ubound < error_range and index < 3:
        index = index + 1
        error_ubound = error_ubound + error_array[index][0]

    if index == 0: error_rel = numpy.random.uniform(0, error_array[index][1])
    else: error_rel = numpy.random.uniform(error_array[index-1][1], error_array[index][1])

    if DEBUG: print("error_range=", error_range, "error_rel=", error_rel)

    t_real_time = t_etime - (t_etime * error_rel)

    if DEBUG: print("t_real_time=", t_etime)

    if current_time == -1:
        print("{0:.0f} {1:.0f}".format(t_etime, t_real_time))
    else:
        print("{0:d} {1:.0f} {2:.0f}".format(current_time, t_etime, t_real_time))
        #print("{0:d} {1:.0f} {2:.0f} {3:d}".format(current_time, t_etime, t_real_time, t_cpu))

def main():
    if len(sys.argv) != 6:
        print("python", sys.argv[0], "<num_tasks> <error_level> <parallelism_level> <arrival_level> <seed>")
        exit(-1)

    num_tasks = int(sys.argv[1])
    seed = int(sys.argv[5])

    error_level = sys.argv[2]
    parallelism_level = sys.argv[3]
    arrival_level = sys.argv[4]
    
    if error_level == 'l':
        error_serial = ERROR_LOW_SERIAL
        error_parallel = ERROR_LOW_PARALLEL
    elif error_level == 'm':
        error_serial = ERROR_MED_SERIAL
        error_parallel = ERROR_MED_PARALLEL
    elif error_level == 'h':
        error_serial = ERROR_HIGH_SERIAL
        error_parallel = ERROR_HIGH_PARALLEL
    elif error_level == 'n':
        error_serial = ERROR_NONE_SERIAL
        error_parallel = ERROR_NONE_PARALLEL    
    else:
        print("ERROR: error_level {0} is invalid. error_level should be l (low), m (medium), h (high) or n (none)".format(error_level))
        exit(-1)
            
    if parallelism_level == 'l':
        parallelism = CHANCE_SERIAL[0]
    elif parallelism_level == 'm':
        parallelism = CHANCE_SERIAL[1]
    elif parallelism_level == 'h':
        parallelism = CHANCE_SERIAL[2]
    else:
        print("ERROR: parallelism_level {0} is invalid. parallelism_level should be l (low), m (medium), or h (high)".format(parallelism_level))
        exit(-1)

    if arrival_level == 'l':
        arrival = ARRIVAL_RATE[0]
    elif arrival_level == 'm':
        arrival = ARRIVAL_RATE[1]
    elif arrival_level == 'h':
        arrival = ARRIVAL_RATE[2]
    elif arrival_level == 'n':
        arrival = ARRIVAL_RATE[3]
    else:
        print("ERROR: arrival_level {0} is invalid. arrival_level should be l (low), m (medium), or h (high)".format(arrival_level))
        exit(-1)

    current_time = 0
    numpy.random.seed(seed)

    if DEBUG:
        print("num_tasks", num_tasks)
        print("seed", seed)

    while num_tasks > 0:
        if arrival != None:
            tasks_arrived = numpy.random.poisson(arrival)
        else:
            tasks_arrived = num_tasks
            current_time = -1

        if DEBUG: print(">> Arrived: ",tasks_arrived)

        if tasks_arrived > 0:
            if num_tasks >= tasks_arrived:
                num_tasks = num_tasks - tasks_arrived
            else:
                tasks_arrived = num_tasks
                num_tasks = 0

            for t in range(tasks_arrived):
                if DEBUG: print(" task ",t)

                t_type = numpy.random.random()
                if DEBUG: print(" t_type ", t_type)

                if t_type <= parallelism:
                    if DEBUG: print("SERIAL")
                    t_cpu = 1
                    generate_task(current_time, 1, ETIME_SERIAL, error_serial)
                else:
                    if DEBUG: print("PARALLEL")
                    t_cpu = numpy.random.random_integers(2, MAX_CPU)
                    generate_task(current_time, t_cpu, ETIME_PARALLEL, error_parallel)

        current_time = current_time + 1

    return 0

if __name__ == '__main__':
    main()

