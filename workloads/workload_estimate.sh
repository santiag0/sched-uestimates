SEED=1

mkdir instances

function generate {
    NUM_INST=$5

    for (( i=0; i<NUM_INST; i++ )) {
        python3 workload_estimate.py ${1} ${2} ${3} ${4} ${SEED} > instances/workload_${1}_${2}_${3}_${4}_${i}.log
        SEED=$(($SEED+1))
    }
}

#TASK_NUM[0]=1024
#TASK_NUM[1]=2048
#TASK_NUM[2]=4096

TASK_NUM[0]=512
TASK_NUM[1]=768
TASK_NUM[2]=1024

ERROR_LEVEL[0]="l"
ERROR_LEVEL[1]="m"
ERROR_LEVEL[2]="h"
ERROR_LEVEL[3]="n"

PARALLELISM_LEVEL[0]="l"
PARALLELISM_LEVEL[1]="m"
PARALLELISM_LEVEL[2]="h"

ARRIVAL_LEVEL[0]="n"
ARRIVAL_LEVEL[1]="m"
ARRIVAL_LEVEL[2]="h"
ARRIVAL_LEVEL[3]="l"

for (( j0=2; j0<3; j0++ )) {
    for (( j1=0; j1<4; j1++ )) {
        for (( j2=0; j2<1; j2++ )) {
            for (( j3=1; j3<3; j3++ )) {
                generate ${TASK_NUM[j0]} ${ERROR_LEVEL[j1]} ${PARALLELISM_LEVEL[j2]} ${ARRIVAL_LEVEL[j3]} 50
            }
        }
    }
}
